var smallPreviewImages = document.querySelectorAll('.small-preview');
var zoomedImage = document.querySelector('.zoomed-image');

for (var i = 0; i < smallPreviewImages.length; i++) {
  smallPreviewImages[i].addEventListener('click', function (e) {
    var elem = e.target;
    zoomedImage.style.backgroundImage = "url('" + elem.src + "')";
  })
}

zoomedImage.addEventListener('mouseenter', function (e) {
  this.style.backgroundSize = "250%";
});

zoomedImage.addEventListener('mousemove', function (e) {
  // getBoundingClientReact gives us various information about the position of the element.
  var dimensions = this.getBoundingClientRect();

  // Calculate the position of the cursor inside the element (in pixels).
  var x = e.clientX - dimensions.x;
  var y = e.clientY - dimensions.y;

  // Calculate the position of the cursor as a percentage of the total size of the element.
  var xPercent = Math.round(100 / (dimensions.width / x));
  var yPercent = Math.round(100 / (dimensions.height / y));
  this.style.backgroundPosition = xPercent + "% " + yPercent + "%";
});

zoomedImage.addEventListener('mouseleave', function (e) {
  this.style.backgroundSize = "cover";
  this.style.backgroundPosition = "center";
});