# Vanilla JavaScript Image Zoom
This is a image zoom example with vanilla JavaScript and CSS Grid System.

All images used in this example were found on [Unsplash](https://unsplash.com) and taken by [Aneta Ivanova](http://anetaivanova.com), [Dan Carlson](http://www.callmedan.com) and [Samuel Scrimshaw](https://instagram.com/samscrim) respectively. 